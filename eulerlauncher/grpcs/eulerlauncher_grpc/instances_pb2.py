# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: eulerlauncher/grpcs/eulerlauncher_grpc/instances.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n6eulerlauncher/grpcs/eulerlauncher_grpc/instances.proto\x12\x08omnivirt\"M\n\x08Instance\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05image\x18\x02 \x01(\t\x12\x10\n\x08vm_state\x18\x03 \x01(\t\x12\x12\n\nip_address\x18\x04 \x01(\t\"\x16\n\x14ListInstancesRequest\">\n\x15ListInstancesResponse\x12%\n\tinstances\x18\x01 \x03(\x0b\x32\x12.omnivirt.Instance\"B\n\x15\x43reateInstanceRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05image\x18\x02 \x01(\t\x12\x0c\n\x04\x61rch\x18\x03 \x01(\t\"j\n\x16\x43reateInstanceResponse\x12\x0b\n\x03ret\x18\x01 \x01(\r\x12\x0b\n\x03msg\x18\x02 \x01(\t\x12)\n\x08instance\x18\x03 \x01(\x0b\x32\x12.omnivirt.InstanceH\x00\x88\x01\x01\x42\x0b\n\t_instance\"%\n\x15\x44\x65leteInstanceRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\"2\n\x16\x44\x65leteInstanceResponse\x12\x0b\n\x03ret\x18\x01 \x01(\r\x12\x0b\n\x03msg\x18\x02 \x01(\t\"H\n\x13TakeSnapshotRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x10\n\x08snapshot\x18\x02 \x01(\t\x12\x11\n\tdest_path\x18\x03 \x01(\t\"#\n\x14TakeSnapshotResponse\x12\x0b\n\x03msg\x18\x01 \x01(\t\"\\\n\x1d\x45xportDevelopmentImageRequest\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\r\n\x05image\x18\x02 \x01(\t\x12\x11\n\tdest_path\x18\x03 \x01(\t\x12\x0b\n\x03pwd\x18\x04 \x01(\t\"-\n\x1e\x45xportDevelopmentImageResponse\x12\x0b\n\x03msg\x18\x01 \x01(\t2\xdd\x03\n\x13InstanceGrpcService\x12S\n\x0elist_instances\x12\x1e.omnivirt.ListInstancesRequest\x1a\x1f.omnivirt.ListInstancesResponse\"\x00\x12V\n\x0f\x63reate_instance\x12\x1f.omnivirt.CreateInstanceRequest\x1a .omnivirt.CreateInstanceResponse\"\x00\x12V\n\x0f\x64\x65lete_instance\x12\x1f.omnivirt.DeleteInstanceRequest\x1a .omnivirt.DeleteInstanceResponse\"\x00\x12P\n\rtake_snapshot\x12\x1d.omnivirt.TakeSnapshotRequest\x1a\x1e.omnivirt.TakeSnapshotResponse\"\x00\x12o\n\x18\x65xport_development_image\x12\'.omnivirt.ExportDevelopmentImageRequest\x1a(.omnivirt.ExportDevelopmentImageResponse\"\x00\x42\x03\x80\x01\x01\x62\x06proto3')

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, 'eulerlauncher.grpcs.eulerlauncher_grpc.instances_pb2', _globals)
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  DESCRIPTOR._serialized_options = b'\200\001\001'
  _globals['_INSTANCE']._serialized_start=68
  _globals['_INSTANCE']._serialized_end=145
  _globals['_LISTINSTANCESREQUEST']._serialized_start=147
  _globals['_LISTINSTANCESREQUEST']._serialized_end=169
  _globals['_LISTINSTANCESRESPONSE']._serialized_start=171
  _globals['_LISTINSTANCESRESPONSE']._serialized_end=233
  _globals['_CREATEINSTANCEREQUEST']._serialized_start=235
  _globals['_CREATEINSTANCEREQUEST']._serialized_end=301
  _globals['_CREATEINSTANCERESPONSE']._serialized_start=303
  _globals['_CREATEINSTANCERESPONSE']._serialized_end=409
  _globals['_DELETEINSTANCEREQUEST']._serialized_start=411
  _globals['_DELETEINSTANCEREQUEST']._serialized_end=448
  _globals['_DELETEINSTANCERESPONSE']._serialized_start=450
  _globals['_DELETEINSTANCERESPONSE']._serialized_end=500
  _globals['_TAKESNAPSHOTREQUEST']._serialized_start=502
  _globals['_TAKESNAPSHOTREQUEST']._serialized_end=574
  _globals['_TAKESNAPSHOTRESPONSE']._serialized_start=576
  _globals['_TAKESNAPSHOTRESPONSE']._serialized_end=611
  _globals['_EXPORTDEVELOPMENTIMAGEREQUEST']._serialized_start=613
  _globals['_EXPORTDEVELOPMENTIMAGEREQUEST']._serialized_end=705
  _globals['_EXPORTDEVELOPMENTIMAGERESPONSE']._serialized_start=707
  _globals['_EXPORTDEVELOPMENTIMAGERESPONSE']._serialized_end=752
  _globals['_INSTANCEGRPCSERVICE']._serialized_start=755
  _globals['_INSTANCEGRPCSERVICE']._serialized_end=1232
# @@protoc_insertion_point(module_scope)
